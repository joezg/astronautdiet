This project doesn't use any framework or library, except [React](https://facebook.github.io/react/) and [Create React App](https://github.com/facebookincubator/create-react-app), because it is indended to show skills in implementing simple, but not so trivial React app.

After cloning the repository, app can be startet like this:
```bash
yarn install
yarn start
```
or
```bash
npm install
npm start
```
Webpack development server will serve the app on [http://localhost:3000](http://localhost:3000).

Project structure:

- public - index.html and images

- src

   - components - react components
 
   - containers - smart react components
 
   - helpers - various helper modules
 
   - hoc - high order components


All UI components are implmented as pure components whose render methods depends only on props and states. All cross-cutting concerns (like data fetching, drag 'n' drop or app wide messaging) are implemented in containers with the help of high order components (hoc).

Because of this UI components could be implemented as pure components for performance benefits. Furthermore UI components are not polluted with cross-cutting implementation specifics and can be easily reused in different context.

Containers are taking care of things like communicating with server API, dragging and dropping and managing app wide messaging by provding UI components with necessarry props.

Containers are created by one high order component or with composition of more high order components. HOCs wrap UI components giving them additional functionality.