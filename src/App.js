import React, { Component } from 'react';
import './App.css';
import AstronautDiet from './containers/AstronautDietContainer';


class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <strong>Astronaut's Diet</strong>
        </div>
        <AstronautDiet />
      </div>
    );
  }
}

export default App;
