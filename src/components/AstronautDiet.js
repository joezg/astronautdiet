import React from 'react';
import Vitamins from './Vitamins';
import Food from '../containers/FoodContainer';
import Meals from '../containers/MealsContainer';
import MessagesContainer from '../containers/MessageContainer';

export default (props) => {
    return (
        <div className="content">
          <Vitamins active={props.todaysVitamins} />
          <div className="food-meals">
            <Food todaysVitamins={props.todaysVitamins} />
            <Meals />
          </div>
          <MessagesContainer />
        </div>
    )
}