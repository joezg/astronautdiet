import React from 'react';
import FoodList from './FoodList';

export default (props) => {
    return (
        <div className="food">
            <h3>Food</h3>
            { props.loading ? null : <FoodList food={props.food} todaysVitamins={props.todaysVitamins}/>}
        </div>
    )
}