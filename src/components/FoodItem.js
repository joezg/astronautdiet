import React from 'react';

export default (props) => {
    const className = (props.featured ? "featured " : "") +
        (props.open ? "open " : "closed ") +
        (props.category ? "food category" : "food item");

    const handleOnClick = () => {
        if (props.onClick){
            props.onClick(props.name);
        }
    }


    return <li onClick={handleOnClick} className={className}>{props.name}</li>
}