import React, { Component } from 'react';
import FoodItem from './FoodItem';
import DraggableFoodItem from '../containers/DraggableFoodItemContainer';

export default class FoodList extends Component {
    constructor(props){
        super(props);
        
        const closedCategories = [];

        for (let food of props.food){
            if (food.children){
                closedCategories.push(food.name);
            }
        }


        this.state = {
            closedCategories
        }

        this.handleOnCategoryClick = this.handleOnCategoryClick.bind(this);
    }

    handleOnCategoryClick(name){
        this.setState((previousState) => {
            const index = previousState.closedCategories.indexOf(name);

            if (index > -1){
                return {
                    closedCategories: [
                        ...previousState.closedCategories.slice(0, index),
                        ...previousState.closedCategories.slice(index+1)
                    ]
                }
            } else {
                return {
                    closedCategories: [
                        ...previousState.closedCategories,
                        name
                    ]
                }
            }
            
        })
    }

    render(){
        return (
            <ul style={this.props.style}>
                {this.props.food.map((foodItem) => {
                    if (foodItem.children){
                        const isOpen = this.state.closedCategories.indexOf(foodItem.name) === -1;
                        return (
                            <div key={foodItem.name}>
                                <FoodItem onClick={this.handleOnCategoryClick} key={foodItem.name} open={isOpen} category name={foodItem.name} />
                                <FoodList 
                                    {...this.props} 
                                    style={{ display: isOpen ? 'inherit' : 'none' }} 
                                    food={foodItem.children} 
                                />
                            </div>
                        );
                    } else {
                        let featured = false;
                        for (let itemVitamin of foodItem.vitamins){
                            if (this.props.todaysVitamins.indexOf(itemVitamin)>-1){
                                featured = true;
                                break;
                            }
                        }

                        return <DraggableFoodItem key={foodItem.code} featured={featured} name={foodItem.name} code={foodItem.code} />
                    }
                })}
            </ul>
        )
    }
}
