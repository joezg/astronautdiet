import React, { Component } from 'react';
import MealsTable from '../containers/MealsTableContainer';
import { dateFormat } from '../helpers/dateHelpers'; 
import { deconstructFoodData } from '../helpers/mealsHelpers';

export default class Meals extends Component{
    constructor(props){
        super(props);

        this.state = {
            numberOfMeals: 0,
            food: {}
        }

        this.handleOnFoodAdd = this.handleOnFoodAdd.bind(this);
        this.handleOnTentativeFoodAdd = this.handleOnTentativeFoodAdd.bind(this);
        this.handleOnTentativeFoodRemove = this.handleOnTentativeFoodRemove.bind(this);
        this.handleMealQuantityChange = this.handleMealQuantityChange.bind(this);
        this.addNewMeal = this.addNewMeal.bind(this);
        this.handleRemoveMeal = this.handleRemoveMeal.bind(this);
        this.handleSubmitClick = this.handleSubmitClick.bind(this);
    }

    addFood(food, previousFood, numberOfMeals, isTentative = false){
        const newFood = { ...food }
        newFood.quantities = new Array(numberOfMeals+1).fill(0);
        
        let propertyName = newFood.code;
        if (isTentative){
            propertyName = "tentative";
            newFood.tentative = true;
        }

        return {
            food: { [propertyName]: newFood, ...previousFood }
        };
    }

    handleOnFoodAdd(food){
        this.setState((previousState) => {
            return this.addFood(food, previousState.food, previousState.numberOfMeals); 
        });
    }

    handleOnTentativeFoodAdd(food){
        let isTentative = true;
        this.setState((previousState) => {
            if (!previousState.food.hasOwnProperty(food.code)){
                return this.addFood(food, previousState.food, previousState.numberOfMeals, isTentative); 
            }
        });
    }

    handleOnTentativeFoodRemove(food){
        this.setState((previousState) => {
            const newState = {
                food: { ...previousState.food }
            }
            delete newState.food.tentative;

            return newState; 
        });
    }

    addNewMeal(newQuantity, currentFoodItem){
        this.setState((previousState) => {
            const newFood = { ...previousState.food }
            for (let code in newFood){
                let foodItem = newFood[code];
                if (currentFoodItem.code === foodItem.code){
                    foodItem.quantities[foodItem.quantities.length-1] = newQuantity;
                }
                foodItem.quantities.push(0);
            }

            return {
                food: newFood,
                numberOfMeals: previousState.numberOfMeals + 1
            }
        })
    }

    handleMealQuantityChange(newQuantity, mealIndex, foodItem){
        if (mealIndex === this.state.numberOfMeals){
            this.addNewMeal(newQuantity, foodItem);
            return;
        }

        this.setState((previousState)=>{
            foodItem.quantities[mealIndex] = newQuantity;

            return {
                food: {
                    ...previousState.food,
                    [foodItem.code]: { ...foodItem }
                }
            }
        })
    }

    handleRemoveMeal(mealIndex){
        this.setState((previousState) => {
            const newFood = { ...previousState.food }
            for (let code in newFood){
                let foodItem = newFood[code];
                foodItem.quantities.splice(mealIndex, 1);
            }

            return {
                food: newFood,
                numberOfMeals: previousState.numberOfMeals - 1
            }
        })
    }

    handleSubmitClick(){
        const deconstructedFood = deconstructFoodData(this.state.food);
        const dailyIntake = [
            deconstructedFood.food.map((food)=>food.code), 
            ...deconstructedFood.meals.slice(0, deconstructedFood.meals.length -1)
        ]

        this.props.submitMeals({
            date: dateFormat(new Date()),
            dailyIntake 
        })
    }

    render() {
        return (
            <div className="meals">
                <h3>Meals</h3>
                <MealsTable 
                    food={this.state.food} 
                    numberOfMeals={this.state.numberOfMeals}
                    onFoodAdd={this.handleOnFoodAdd}
                    onTentativeFoodAdd={this.handleOnTentativeFoodAdd}
                    onTentativeFoodRemove={this.handleOnTentativeFoodRemove}
                    onMealQuantityChange={this.handleMealQuantityChange}
                    onRemoveMeal={this.handleRemoveMeal}
                />
                <button onClick={this.handleSubmitClick} disabled={this.state.numberOfMeals < 1} className="save-meals">Save meals</button>
            </div>
        )
    }
}