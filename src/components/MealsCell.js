import React, { Component } from 'react';

export default class MealsCell extends Component{
    constructor(props){
        super(props);

        this.state = {
            visibleAll: false
        }

        this.handleMouseEnterAndLeave = this.handleMouseEnterAndLeave.bind(this);
        this.handleIncreaseClick = this.handleIncreaseClick.bind(this);
        this.handleDecreaseClick = this.handleDecreaseClick.bind(this);
    }

    handleMouseEnterAndLeave(){
        this.setState((previousState) => ({
            visibleAll: !previousState.visibleAll
        }));
    }

    handleIncreaseClick(){
        this.props.onValueChange && this.props.onValueChange(this.props.quantity + 1, this.props.index);
    }

    handleDecreaseClick(){
        this.props.onValueChange && this.props.onValueChange(this.props.quantity - 1, this.props.index);
    }

    render() {
        return (
            <div onMouseEnter={this.handleMouseEnterAndLeave} onMouseLeave={this.handleMouseEnterAndLeave} className="meals-cell">
                { this.props.quantity || this.state.visibleAll ? <div className="counter meals-quantity">{this.props.quantity}</div> : null}
                { this.state.visibleAll ? <div onClick={this.handleDecreaseClick} className="decrease meals-quantity" /> : null }                
                { this.state.visibleAll ? <div onClick={this.handleIncreaseClick} className="increase meals-quantity" /> : null }
            </div>
        )
    }
}