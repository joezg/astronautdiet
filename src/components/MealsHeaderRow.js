import React from 'react';

export default (props) => {
    return (
        <div className="meals-row">
            <div className="remove meal" />
            {props.food.map((foodItem)=>{
                return <div key={foodItem.name} className="meals-cell">{foodItem.name}</div>
            })}
            { props.tentativeFood ? <div className="future meals-cell">{props.tentativeFood.name}</div> : null }
        </div>
    )
}