import React from 'react';
import MealsCell from './MealsCell';
import RemoveMeal from './RemoveMeal';

export default (props) => {
    const selectableClass = props.nonSelectable ? "" : "selectable";
    const handleValueChange = (newQuantity, foodIndex) => {
        props.onValueChange && props.onValueChange(newQuantity, props.index, foodIndex)
    }

    return (
        <div className={selectableClass + " meals-row"}>
            <RemoveMeal index={props.index} onRemove={props.onRemove} />
            { props.quantities.map((quantity, i)=>{
                return <MealsCell onValueChange={handleValueChange} key={i} index={i} className="meals-cell" quantity={quantity} />
            }) }
            
            { props.tentativeFood ? <div className="future meals-cell"></div> : null }
        </div>
    )
}