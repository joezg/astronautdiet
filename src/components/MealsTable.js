import React from 'react';
import MealsHeaderRow from './MealsHeaderRow';
import MealsRow from './MealsRow';
import { deconstructFoodData } from '../helpers/mealsHelpers';

export default (props)=>{
    const { food, meals, tentativeFood } = deconstructFoodData(props.food); 

    const handleValueChange = (newValue, mealIndex, foodIndex) => {
        props.onMealQuantityChange && props.onMealQuantityChange(newValue, mealIndex, food[foodIndex]);
    }

    const renderTable = () => {
        return (
            <div className="meals-table">
                <MealsHeaderRow 
                    food={food} 
                    tentativeFood={tentativeFood} 
                />
                { 
                    meals.map((meal, i)=>{
                        return <MealsRow onValueChange={handleValueChange} 
                                    onRemove={props.onRemoveMeal}
                                    nonSelectable={i===props.numberOfMeals}
                                    key={i}
                                    index={i}
                                    quantities={meal} 
                                    tentativeFood={tentativeFood} 
                                />
                    })
                }            
            </div>
        )
    }

    if (food.length===0 && !tentativeFood){
        return <div className="meals-placeholder">Drag food here to start recording meals!</div>;
    } else {
        return renderTable();
    }
    
        
}