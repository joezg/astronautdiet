import React from 'react';

export default (props) => {
    const handleRemoveClick = () => {
        props.removeMessage(props.message);
    }

    return (
        <div className={props.message.level + " message"}>
            <div style={{flexGrow: 30}}>{props.message.text}</div>
            <div onClick={handleRemoveClick} style={{flexGrow: 1, cursor: "pointer"}}>X</div>
        </div>
    )
}