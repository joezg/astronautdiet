import React from 'react';
import Message from './Message'

export default (props) => {
    const renderMessages = () => {
        return props.messages.map((message, i) => {
            return <Message key={i} message={message} removeMessage={props.removeMessage} />
        })
    }

    return (
        <div className="messages">
            { renderMessages() }
        </div>
    )

}