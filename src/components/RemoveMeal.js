import React, { Component } from 'react';

export default class RemoveMeal extends Component{
    constructor(props){
        super(props);

        this.state = {
            pending: false
        };

        this.handlePendingToggle = this.handlePendingToggle.bind(this);
        this.handleAcceptClick = this.handleAcceptClick.bind(this);
    }

    handleAcceptClick(){
        this.props.onRemove && this.props.onRemove(this.props.index);
        this.setState((previousState) => {
            return {
                pending: !previousState.pending
            }
        })
    }

    handlePendingToggle(){
        this.setState((previousState) => {
            return {
                pending: !previousState.pending
            }
        })
    }

    render(){
        if (!this.state.pending){
            return <div onClick={this.handlePendingToggle} className="remove meal" />
        } else {
            return (
                <div className="confirm remove meal" onMouseLeave={this.handlePendingToggle}>
                    <div onClick={this.handleAcceptClick} className="accept"/>
                    <div onClick={this.handlePendingToggle} className="reject"/>
                </div>
            )
        }
        
    }
}