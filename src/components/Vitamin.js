import React from 'react';

export default (props) => {
    let b1Class = props.code === 2 ? "b1" : "";
    let inactive = props.active ? "" : "inactive"

    return <span className={ b1Class + " vitamin " + inactive}>{props.name}</span>
}