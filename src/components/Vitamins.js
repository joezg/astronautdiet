import React from 'react';
import Vitamin from './Vitamin';

export default (props) => {
    const vitamins = [
        {code: 1, name: "A", active: props.active.indexOf(1) > -1},
        {code: 2, name: "B1", active: props.active.indexOf(2) > -1},
        {code: 3, name: "C", active: props.active.indexOf(3) > -1},
        {code: 4, name: "D", active: props.active.indexOf(4) > -1},
        {code: 5, name: "E", active: props.active.indexOf(5) > -1},
    ]

    return (
        <div className="today-vitemins">
            Vitamins for today: 
            {vitamins.map((vitamin)=>{
                return <Vitamin key={vitamin.code} {...vitamin} />;
            })}
        </div>
    )
}