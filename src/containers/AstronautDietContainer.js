import AstronautDiet from '../components/AstronautDiet';
import ajaxContainer from '../hoc/ajaxContainer';

const url = (props) => { 
    const currentDate = new Date(),
        currentDay = currentDate.getDay() + 1;
    return "https://private-anon-9a8af97af6-spacemission.apiary-mock.com/dailyvitamins/" + currentDay;
}

const mapResultToProps = (result, ownProps) => {
    const props = {
        todaysVitamins: []
    }
    if (result.data){
        for (let vitamin of result.data){
            props.todaysVitamins.push(vitamin.code);
        }
    }
    return props;
}

const addLoadingOverlay = true;

const fetchingError = (error) => {
    return "Error while fetching daily vitamins. Please try again later or contact help desk.";
}

export default ajaxContainer(url, mapResultToProps, addLoadingOverlay, fetchingError)(AstronautDiet);