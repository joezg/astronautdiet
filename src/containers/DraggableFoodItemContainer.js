import FoodItem from '../components/FoodItem';
import draggableContainer from '../hoc/draggableContainer';

const setDragData = (props) => {
    return {
        code: props.code,
        name: props.name
    }
}

export default draggableContainer(setDragData)(FoodItem);