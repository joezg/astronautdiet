import Food from '../components/Food';
import ajaxContainer from '../hoc/ajaxContainer';

const url = "https://private-anon-9a8af97af6-spacemission.apiary-mock.com/food";

const mapResultToProps = (result, ownProps) => {
    return {
        food: result.data ? result.data : []
    }
}

export default ajaxContainer(url, mapResultToProps)(Food);