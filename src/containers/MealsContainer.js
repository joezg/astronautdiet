import ajaxPostManager from '../hoc/ajaxPostManager';
import Meals from '../components/Meals.js'

const url = "https://private-anon-9a8af97af6-spacemission.apiary-mock.com/dailyintake";
const postPropName = "submitMeals";
const successMessage = "Meals submitted";
const errorMessage = "Error occured while submitting meals."
export default ajaxPostManager(url, postPropName, successMessage, errorMessage)(Meals);