import MealsTable from '../components/MealsTable'
import droppableContainer from '../hoc/droppableContainer';

const mapEnterDataToProps = (data, ownProps) => {
    ownProps.onTentativeFoodAdd(data);
    return {}
};

const resetEnterDataProps = (ownProps) => {
    ownProps.onTentativeFoodRemove();
    return {}
};

const mapDropDataToProps = (data, ownProps) => {
    ownProps.onTentativeFoodRemove();
    ownProps.onFoodAdd(data);
    return {};
};

export default droppableContainer(mapEnterDataToProps, mapDropDataToProps, resetEnterDataProps, null)(MealsTable);