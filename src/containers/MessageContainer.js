import messagesContainer from '../hoc/messagesContainer';
import messagesManager from '../hoc/messagesManager';
import { addMessage } from '../helpers/messages';
import Messages from '../components/Messages';

addMessage({text: "Welcome!", level: "info", expire: 5000});

const mapMessagesMethodsToProps = ({removeMessage}) => ({
    removeMessage
});

export default 
    messagesManager(mapMessagesMethodsToProps)(
        messagesContainer()(Messages)
    );