let currentDragData = {}

export const pushData = (newData) => {
    currentDragData = newData
}

export const getData = () => {
    return currentDragData;
}

export const popData = () => {
    const actualData = currentDragData;
    currentDragData = {};

    return actualData;
}