let getObjectValues = (obj) => {
    const arr = [];
    for (let prop in obj){
        if (obj.hasOwnProperty(prop)){
            arr.push(obj[prop]);
        }
    }
    return arr;
}

export const deconstructFoodData = (food) => {
    const foodArray = [];
    const meals = [];
    let tentativeFood = null;

    for (let foodItem of getObjectValues(food)){
        if (foodItem.tentative){
            tentativeFood = foodItem;
        } else {
            foodArray.push(foodItem);
            for (let i=0; i<foodItem.quantities.length; i++){
                if (!meals[i]){
                    meals[i] = [];
                }
                meals[i].push(foodItem.quantities[i]);
            }
        }
        
    }
    return {
        food: foodArray, meals, tentativeFood
    };
}