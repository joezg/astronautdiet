const messages = [];
const subscriptions = [];

const publish = () => {
    for (let sub of subscriptions){
        if (typeof sub === 'function'){
            sub(messages);
        }        
    }
}

export const getMessages = () => messages;
export const subscribe = (cb) => {
    const handle = subscriptions.length;
    subscriptions.push(cb);
    return handle;
}
export const unsubscribe = (handle) => {
    subscriptions.splice(handle, 1);
}
export const addMessage = ({ text, level, expire }) => {
    const newMessage = { text, level };
    messages.push(newMessage);

    if (expire){
        setTimeout(()=>{
            removeMessage(newMessage);
        }, expire);
    }

    publish();
}
export const removeMessage = (message) => {
    messages.splice(messages.indexOf(message), 1);
    publish();
}
