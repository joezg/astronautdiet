import React, { Component } from 'react';
import Loading from '../components/Loading';
import { addMessage } from '../helpers/messages';

export default (url, mapResultToProps, addLoadingOverlay, fetchingErrorMessage) => {
    return (ChildComponent) => {
        return class AjaxContainer extends Component {
            constructor(props){
                super(props);

                let endPoint = url;
                if (typeof url === 'function'){
                    endPoint = url(props);
                }

                this.state = {
                    ...mapResultToProps({}, props),
                    loading: true
                };

                var xhr = new XMLHttpRequest();

                xhr.onreadystatechange = () => {
                    if (xhr.readyState === 4) { //4 => done
                        if (xhr.status === 200) {
                            this.setState({
                                ...mapResultToProps(JSON.parse(xhr.responseText), props),
                                loading: false
                            });
                        } else {
                            let message = "Error while fetching data.";
                            if (fetchingErrorMessage){
                                message = typeof fetchingErrorMessage === 'function' ? fetchingErrorMessage(JSON.parse(xhr.response)) : fetchingErrorMessage;
                            }
                            
                            addMessage({ text: message, level:"error"});
                            this.setState({
                                loading: false
                            })
                        }
                    }
                    
                };

                xhr.open('GET', endPoint);
                xhr.send(null);
            }

            render(){
                if (addLoadingOverlay){
                    return (
                        <div>
                            { this.state.loading ? <Loading /> : null }    
                            <ChildComponent {...this.props} {...this.state} />
                        </div>
                    )
                } else {
                    return <ChildComponent {...this.props} {...this.state} />
                }
                
            }
        }
    }
}