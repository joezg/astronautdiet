import React, { Component } from 'react';
import { addMessage } from '../helpers/messages';

export default (url, postPropName, successMessage, errorMessage) => {
    return (ChildComponent) => {
        return class AjaxPostManager extends Component {
            constructor(props){
                super(props);

                this.state = {
                    [postPropName]: this.handlePostStart.bind(this)
                }
            }

            handlePostStart(data){
                let endPoint = url;
                if (typeof url === 'function'){
                    endPoint = url(this.props);
                }

                var xhr = new XMLHttpRequest();

                xhr.onreadystatechange = () => {
                    if (xhr.readyState === 4) { //4 => done
                        if (xhr.status === 200) {
                            let message = "Success!";
                            if (successMessage){
                                message = typeof successMessage === 'function' ? successMessage(JSON.parse(xhr.responseText)) : successMessage;
                            }
                            addMessage({ text: message, level:"info"});
                        } else {
                            let message = "Error while posting data.";
                            if (errorMessage){
                                message = typeof errorMessage === 'function' ? errorMessage(JSON.parse(xhr.response)) : errorMessage;
                            }
                            
                            addMessage({ text: message, level:"error"});
                        }
                    }
                    
                };

                xhr.open('POST', endPoint);

                xhr.send(JSON.stringify(data));
            }

            render(){
                return <ChildComponent {...this.props} {...this.state} />
            }
        }
    }
}