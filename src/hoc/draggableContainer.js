import React, { Component } from 'react';
import { pushData } from '../helpers/dragData';

export default (getDragData) => {
    return (ChildComponent) => {
        return class DraggableContainer extends Component {
            constructor(props){
                super(props);

                this.handleDragStart = this.handleDragStart.bind(this);
            }

            handleDragStart(ev){
                const dragData = getDragData(this.props);
                //ev.dataTransfer data is not available in dragEnter event because data
                //is protected so I needed to add my own dragData module for handling
                //drag data.
                pushData(dragData);
                for (let data in dragData){
                    ev.dataTransfer.setData(data, dragData[data]);
                }
            }

            render(){
                return (
                    <div draggable="true" onDragStart={this.handleDragStart}>
                        <ChildComponent {...this.props} />
                    </div>
                );
            }
        }
    }
}