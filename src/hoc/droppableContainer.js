import React, { Component } from 'react';
import { getData, popData } from '../helpers/dragData';

export default (mapEnterDataToProps, mapDropDataToProps, resetEnterDataProps, containerCssClass) => {
    return (ChildComponent) => {
        return class DroppableContainer extends Component {
            constructor(props){
                super(props);

                this.state = {
                    dragEntersCount: 0,
                    childProps: {}
                };

                this.handleDragOver = this.handleDragOver.bind(this);
                this.handleDragEnter = this.handleDragEnter.bind(this);
                this.handleDragLeave = this.handleDragLeave.bind(this);
                this.handleDrop = this.handleDrop.bind(this);
            }

            handleDragOver(ev){
                ev.preventDefault();
                ev.dataTransfer.dropEffect = "link";
            }

            handleDragEnter(ev){
                ev.preventDefault();
                this.setState((prevState) => {
                    const dragEntersCount = prevState.dragEntersCount + 1;
                    if (dragEntersCount === 1){
                        return {
                            dragEntersCount,
                            childProps: mapEnterDataToProps(getData(), this.props)
                        }
                    } else {
                        return { dragEntersCount }
                    }
                });
            }

            handleDragLeave(ev){
                ev.preventDefault();
                this.setState((prevState) => {
                    const dragEntersCount = prevState.dragEntersCount - 1;
                    if (dragEntersCount === 0){
                        return {
                            dragEntersCount,
                            childProps: resetEnterDataProps(this.props)
                        }
                    } else {
                        return { dragEntersCount };
                    }
                });
            }

            handleDrop(ev){
                ev.preventDefault();
                this.setState({
                    childProps: mapDropDataToProps(popData(), this.props),
                    dragEntersCount: 0
                });
            }

            render(){
                return (
                    <div className={containerCssClass} 
                        onDragOver={this.handleDragOver} 
                        onDragEnter={this.handleDragEnter} 
                        onDrop={this.handleDrop} 
                        onDragLeave={this.handleDragLeave}
                    >
                        <ChildComponent {...this.props} {...this.state.childProps} />
                    </div>
                );
            }
        }
    }
}