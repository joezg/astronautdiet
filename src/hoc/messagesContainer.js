import React, { Component } from 'react';
import { getMessages, subscribe } from '../helpers/messages';

export default () => {
    return (ChildComponent) => {
        return class MessagesContainer extends Component{
            constructor(props){
                super(props);

                this.state = {
                    messages: getMessages()
                }

                subscribe(this.onMessagesChange.bind(this));
            }

            onMessagesChange(messages){
                this.setState({
                    messages
                });
            }

            render(){
                return <ChildComponent messages={this.state.messages} {...this.props} />
            }
        }
    }
}