import React, { Component } from 'react';
import * as messagesMethods from '../helpers/messages';

export default (mapMethodsToProps) => {
    return (ChildComponent) => {
        return class MessagesManager extends Component{
            constructor(props){
                super(props);

                this.state = {
                    ...mapMethodsToProps(messagesMethods)
                }
            }

            render(){
                return <ChildComponent {...this.state} {...this.props} />
            }
        }
    }
}